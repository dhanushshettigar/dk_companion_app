import 'package:flutter/material.dart';

class CustomScaffold extends StatefulWidget {
  CustomScaffold({this.child});
  final Widget child;
  @override
  _CustomScaffoldState createState() => _CustomScaffoldState();
}

class _CustomScaffoldState extends State<CustomScaffold> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Stack(
      children: [
        Scaffold(
          body: Container(
            width: size.width,
            height: size.height,
            decoration: BoxDecoration(
                image: DecorationImage(
              fit: BoxFit.cover,
              image: AssetImage('assets/images/BG.png'),
            )),
            child: widget.child,
          ),
        ),
      ],
    );
  }
}
