package com.example.dk_companion;

import androidx.appcompat.app.AppCompatActivity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Switch;
import android.view.View;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Set;
import java.util.UUID;
//import java.util.logging.Handler;

public class MainActivity extends AppCompatActivity implements
        AdapterView.OnItemSelectedListener {
    String macAddress;
    String Selected_mac_Address;
    boolean can_connect=false;
    public static boolean connected=false;
    public static boolean cannot_send=false;
    boolean switch_on=true;
    public static BluetoothSocket mmSocket;
    public static ConnectedThread connectedThread;
    public static CreateConnectThread createConnectThread;
    Switch enable_switch,sleep_switch,read_switch,come_in_switch,do_not_switch;
    ArrayList list = new ArrayList();
    ArrayList MAC = new ArrayList();
    final BluetoothAdapter bAdapter = BluetoothAdapter.getDefaultAdapter();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_main);
        enable_switch = (Switch) findViewById(R.id.switch1);
        sleep_switch=(Switch) findViewById(R.id.switch_sleep);
        read_switch=(Switch) findViewById(R.id.switch_read);
        come_in_switch=(Switch) findViewById(R.id.switch_come_in);
        do_not_switch=(Switch) findViewById(R.id.switch_do_not);
        Button connect = (Button) findViewById(R.id.button_connect_bt);
        sleep_switch.setEnabled(false);
        read_switch.setEnabled(false);
        come_in_switch.setEnabled(false);
        do_not_switch.setEnabled(false);
        enable_switch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (enable_switch.isChecked()) {
                    Bluetooth_List();
                    if (!bAdapter.isEnabled()) {
                        startActivityForResult(new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE), 1);
                    }
                    Toast.makeText(getApplicationContext(), "Bluetooth Turned ON", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    bAdapter.disable();
                    Toast.makeText(getApplicationContext(), "Bluetooth Turned OFF", Toast.LENGTH_SHORT).show();
                }
            }
        });
        connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(switch_on==true)
                {
                    if(can_connect==true)
                    {
                        Bluetooth_Connection();
                        Toast.makeText(getApplicationContext(),"Connecting....", Toast.LENGTH_LONG).show();
                        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (connected == true)
                                {
                                    Toast.makeText(getApplicationContext(), "Connected", Toast.LENGTH_SHORT).show();
                                    connect.setText("DISCONNECT"); //device connection
                                    sleep_switch.setEnabled(true);
                                    read_switch.setEnabled(true);
                                    come_in_switch.setEnabled(true);
                                    do_not_switch.setEnabled(true);
                                    switch_on = false;
                                }
                                else
                                {
                                    Toast.makeText(getApplicationContext(), "Cannot connect to device", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }, 5000);
                    }
                    else
                    {
                        Toast.makeText(getApplicationContext(), "Please Select a Device", Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    connectedThread.cancel();
                    connect.setText("CONNECT");
                    sleep_switch.setChecked(false);
                    read_switch.setChecked(false);
                    come_in_switch.setChecked(false);
                    do_not_switch.setChecked(false);
                    sleep_switch.setEnabled(false);
                    read_switch.setEnabled(false);
                    come_in_switch.setEnabled(false);
                    do_not_switch.setEnabled(false);
                    switch_on = true;
                }
            }
        });
        sleep_switch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sleep_switch.isChecked()) {
                    read_switch.setChecked(false);
                    come_in_switch.setChecked(false);
                    do_not_switch.setChecked(false);
                    connectedThread.write("B");
                    if(cannot_send == true)
                    {
                        Toast.makeText(getApplicationContext(), "Cannot Send Data", Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        Toast.makeText(getApplicationContext(), "Sent Data", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        read_switch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (read_switch.isChecked()) {
                    sleep_switch.setChecked(false);
                    come_in_switch.setChecked(false);
                    do_not_switch.setChecked(false);
                    connectedThread.write("Y");
                    if(cannot_send == true)
                    {
                        Toast.makeText(getApplicationContext(), "Cannot Send Data", Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        Toast.makeText(getApplicationContext(), "Sent Data", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        come_in_switch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (come_in_switch.isChecked()) {
                    sleep_switch.setChecked(false);
                    read_switch.setChecked(false);
                    do_not_switch.setChecked(false);
                    connectedThread.write("G");
                    if(cannot_send == true)
                    {
                        Toast.makeText(getApplicationContext(), "Cannot Send Data", Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        Toast.makeText(getApplicationContext(), "Sent Data", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        do_not_switch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (do_not_switch.isChecked()) {
                    sleep_switch.setChecked(false);
                    read_switch.setChecked(false);
                    come_in_switch.setChecked(false);
                    connectedThread.write("R");
                    if(cannot_send == true)
                    {
                        Toast.makeText(getApplicationContext(), "Cannot Send Data", Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        Toast.makeText(getApplicationContext(), "Sent Data", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

//    private void socket_status() {
//        boolean stat =  mmSocket.isConnected();
//        String stats=Boolean.toString(stat);
//        Toast.makeText(getApplicationContext(),stats, Toast.LENGTH_LONG).show();
//    }

    private void Bluetooth_Connection() {
        if (bAdapter.isEnabled()) {
            BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            createConnectThread = new CreateConnectThread(bluetoothAdapter,Selected_mac_Address);
            createConnectThread.start();
        }
    }
    private void Bluetooth_List() {
        Set<BluetoothDevice> pairedDevices = bAdapter.getBondedDevices();
        if(pairedDevices.size()>0) {
            for (BluetoothDevice device : pairedDevices) {
                String device_name = device.getName();
                macAddress = device.getAddress();
                list.add(device_name);
                MAC.add(macAddress);
            }
        }
        Spinner spin = (Spinner) findViewById(R.id.spinner);
        spin.setOnItemSelectedListener(this);
        ArrayAdapter aa = new ArrayAdapter(this,android.R.layout.simple_spinner_item,list);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin.setAdapter(aa);
    }

    public static class CreateConnectThread extends Thread {

        private static final String TAG = "d";
        boolean conn=false;
        public CreateConnectThread(BluetoothAdapter bluetoothAdapter, String address) {
            /*
            Use a temporary object that is later assigned to mmSocket
            because mmSocket is final.
             */
            BluetoothDevice bluetoothDevice = bluetoothAdapter.getRemoteDevice(address);
            BluetoothSocket tmp = null;
            UUID uuid = bluetoothDevice.getUuids()[0].getUuid();

            try {
                /*
                Get a BluetoothSocket to connect with the given BluetoothDevice.
                Due to Android device varieties,the method below may not work fo different devices.
                You should try using other methods i.e. :
                tmp = device.createRfcommSocketToServiceRecord(MY_UUID);
                 */
                tmp = bluetoothDevice.createInsecureRfcommSocketToServiceRecord(uuid);

            } catch (IOException e) {
                Log.e(TAG, "Socket's create() method failed", e);
            }
            mmSocket = tmp;
        }

        public void run() {
            // Cancel discovery because it otherwise slows down the connection.
            BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            bluetoothAdapter.cancelDiscovery();
            try {
                // Connect to the remote device through the socket. This call blocks
                // until it succeeds or throws an exception.
                mmSocket.connect();
                connected=true;
                Log.e("Status", "Device connected");
            } catch (IOException connectException) {
                //connected=false;
                // Unable to connect; close the socket and return.
                try {
                    mmSocket.close();
                    connected=false;
                    Log.e("Status", "Cannot connect to device");
                } catch (IOException closeException) {
                    Log.e(TAG, "Could not close the client socket", closeException);
                }
                return;
            }

            // The connection attempt succeeded. Perform work associated with
            // the connection in a separate thread.
            connectedThread = new ConnectedThread(mmSocket);
            connectedThread.run();
        }

        // Closes the client socket and causes the thread to finish.
        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "Could not close the client socket", e);
            }
        }
    }

    /* =============================== Thread for Data Transfer =========================================== */
    public static class ConnectedThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        public ConnectedThread(BluetoothSocket socket) {
            mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            // Get the input and output streams, using temp objects because
            // member streams are final
            try {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) { }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        public void run() {
            byte[] buffer = new byte[1024];  // buffer store for the stream
            int bytes = 0; // bytes returned from read()
            // Keep listening to the InputStream until an exception occurs
            while (true) {
                try {
                    /*
                    Read from the InputStream from Arduino until termination character is reached.
                    Then send the whole String message to GUI Handler.
                     */
                    buffer[bytes] = (byte) mmInStream.read();
                    String readMessage;
                    if (buffer[bytes] == '\n'){
                        readMessage = new String(buffer,0,bytes);
                        Log.e("Arduino Message",readMessage);
                        bytes = 0;
                    } else {
                        bytes++;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    break;
                }
            }
        }

        /* Call this from the main activity to send data to the remote device */
        public void write(String input) {
            byte[] bytes = input.getBytes(); //converts entered String into bytes
            try {
                mmOutStream.write(bytes);
                cannot_send = false;
            } catch (IOException e) {
                cannot_send = true;
                Log.e("Send Error","Unable to send message",e);
            }
        }

        /* Call this from the main activity to shutdown the connection */
        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) { }
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Selected_mac_Address = (String) MAC.get(position);
        can_connect = true;
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        can_connect = false;
    }
}
